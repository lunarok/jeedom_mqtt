<?php

/* This file is part of Jeedom.
*
* Jeedom is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Jeedom is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Jeedom. If not, see <http://www.gnu.org/licenses/>.
*/

require_once dirname(__FILE__) . '/../../../core/php/core.inc.php';
include_file('core', 'authentification', 'php');
if (!isConnect()) {
    include_file('desktop', '404', 'php');
    die();
}
?>


<form class="form-horizontal">
  <div class="form-group">
    <fieldset>
    <?php

    echo '<div class="form-group">';
    $mosqHost = config::byKey('mqttAdress', 'MQTT', 0);
    if ($mosqHost == '') {
      $mosqHost = '127.0.0.1';
    }
    $mosqPort = config::byKey('mqttPort', 'MQTT', 0);
    if ($mosqPort == '') {
      $mosqPort = '1883';
    }
    $cron = cron::byClassAndFunction('MQTT', 'daemon');
    $status = $cron->running();
    $socket = socket_create(AF_INET, SOCK_STREAM, 0);
    $server = socket_connect ($socket , $mosqHost, $mosqPort);

    echo '<label class="col-sm-4 control-label">{{Cron}}</label>';
  if (!$status) {
  	echo '<div class="col-sm-1"><span class="label label-danger tooltips" style="font-size : 1em;" title="{{Le cron MQTT n\'est pas lancé}}">NOK</span></div>';
  } else {
  	echo '<div class="col-sm-1"><span class="label label-success" style="font-size : 1em;">OK</span></div>';
  }
  echo '<label class="col-sm-1 control-label">{{Broker}}</label>';
  if (!$server) {
  	echo '<div class="col-sm-1"><span class="label label-danger tooltips" style="font-size : 1em;" title="{{Mosquitto est injoignable}}">NOK</span></div>';
  } else {
  	echo '<div class="col-sm-1"><span class="label label-success" style="font-size : 1em;">OK</span></div>';
  }
  echo '</div>';


      echo '</div>';
      echo '<legend><i class="fa fa-cog"></i> {{Paramétrage}}</legend>';

  ?>


    <div class="form-group">
            <label class="col-lg-4 control-label">{{IP de Mosquitto : }}</label>
            <div class="col-lg-4">
				<input id="mosquitto_por" class="configKey form-control" data-l1key="mqttAdress" style="margin-top:5px" placeholder="127.0.0.1"/>
            </div>
        </div>
    <div class="form-group">
            <label class="col-lg-4 control-label">{{Port de Mosquitto : }}</label>
            <div class="col-lg-4">
				<input id="mosquitto_por" class="configKey form-control" data-l1key="mqttPort" style="margin-top:5px" placeholder="1883"/>
            </div>
        </div>

        <div class="form-group">
                <label class="col-lg-4 control-label">{{Identifiant de Connexion : }}</label>
                <div class="col-lg-4">
    				<input id="mosquitto_por" class="configKey form-control" data-l1key="mqttId" style="margin-top:5px" placeholder="Jeedom"/>
                </div>
            </div>
        <div class="form-group">
                <label class="col-lg-4 control-label">{{Compte de Connexion (non obligatoire) : }}</label>
                <div class="col-lg-4">
    				<input id="mosquitto_por" class="configKey form-control" data-l1key="mqttUser" style="margin-top:5px" placeholder="Jeedom"/>
                </div>
            </div>
            <div class="form-group">
                    <label class="col-lg-4 control-label">{{Mot de passe de Connexion (non obligatoire) : }}</label>
                    <div class="col-lg-4">
        				<input id="mosquitto_por" type="password" class="configKey form-control" data-l1key="mqttPass" style="margin-top:5px" placeholder="Jeedom"/>
                    </div>
                </div>


				<div class="alert alert-success"><b>{{Sauvegarde}} : </b>{{La sauvegarde de la configuration redémarre automatiquement le service}}</div>' ;

			<script>

      $('.bt_installDeps').on('click',function(){
  bootbox.confirm('{{Etes-vous sûr de vouloir réinstaller les dépendances ? Celles ci sont normalement installées à l\'activation, cette fonction ne sert que pour le debug et import de sauvegarde }}', function (result) {
    if (result) {
      $.ajax({// fonction permettant de faire de l'ajax
            type: "POST", // méthode de transmission des données au fichier php
            url: "plugins/MQTT/core/ajax/MQTT.ajax.php", // url du fichier php
            data: {
                action: "installDep",
            },
            dataType: 'json',
            global: false,
            error: function (request, status, error) {
                handleAjaxError(request, status, error);
            },
            success: function (data) { // si l'appel a bien fonctionné
                if (data.state != 'ok') {
                    $('#div_alert').showAlert({message: data.result, level: 'danger'});
                    return;
                }
            }
        });
    }
  });
  });

	     function MQTT_postSaveConfiguration(){
             $.ajax({// fonction permettant de faire de l'ajax
            type: "POST", // methode de transmission des données au fichier php
            url: "plugins/MQTT/core/ajax/MQTT.ajax.php", // url du fichier php
            data: {
                action: "postSave",
            },
            dataType: 'json',
            error: function (request, status, error) {
                handleAjaxError(request, status, error);
            },
            success: function (data) { // si l'appel a bien fonctionné
            if (data.state != 'ok') {
                $('#div_alert').showAlert({message: data.result, level: 'danger'});
                return;
            }
            $('#ul_plugin .li_plugin[data-plugin_id=MQTT]').click();
        }
    });

		}


			</script>

    </fieldset>
</form>
